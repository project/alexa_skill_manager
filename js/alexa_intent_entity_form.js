(function ($) {
  Drupal.behaviors.alexaIntentEntityForm = {
    attach: function (context, settings) {
      //console.log('iamin');
      Drupal.behaviors.alexaIntentEntityForm.initializeWidget();
    },
    initializeWidget: function(){
      let markup = '';
      let data = '';
      if($('input[name="slots"]').val().length > 0){
        data = JSON.parse($('input[name="slots"]').val());
      } else {
        data = {};
      }
      markup += '<label class="form-required">'+Drupal.t('Intent Data')+'</label>';
      markup += '<fieldset style="margin-top:0;">';
      markup += Drupal.behaviors.alexaIntentEntityForm.getAlexaTable(data);
      markup += Drupal.behaviors.alexaIntentEntityForm.getDrupalTable(data);
      markup += Drupal.behaviors.alexaIntentEntityForm.getAssociationsTable(data);
      markup += '</fieldset>';
      $('input[name="slots"]').before(markup);
      Drupal.behaviors.alexaIntentEntityForm.addEvents();
    },
    getAlexaTable: function(data){
      let markup='<table class="bridge-table alexa-slots-table" rel="alexa">';
      markup+='<thead><tr>';
      markup+='<th>'+Drupal.t('Alexa slot name')+'</th>';
      markup+='<th>'+Drupal.t('Alexa slot type')+'</th>';
      markup+='<th>'+Drupal.t('Remove')+'</th>';
      markup+='</tr></thead><tbody>';
      //console.log(data);
      let existsrow=false;
      if (typeof data.alexa !== "undefined") {
        for (const [key, value] of Object.entries(data.alexa)) {
          //console.log(key, value);
          for (const [key2, value2] of Object.entries(value)) {
            //console.log(key2, value2);
            existsrow = true;
            markup += '<tr>';
            markup += '<td><input size="40" class="form-text" type="text" rel="key" value="' + key2 + '"></td>';
            markup += '<td><input size="40" class="form-text" type="text" rel="val" value="' + value2 + '">'+'<p class="suggestion-custom">'+Drupal.t('for custom slots you can insert the following expression:DRUPAL.CUSTOM:{entity_type}:{field} <br/> e.g. DRUPAL.CUSTOM:commerce_product_variation:title')+'</p>'+'</td>';
            markup += '<td>' + '<span class="button remove-alexa-row-button">' + Drupal.t('remove row') + '</span>' + '</td>';
            markup += '</tr>';
          }
        }
      }
      if(!existsrow){
        markup+='<tr>';
        markup+='<td><input size="40" class="form-text" type="text" rel="key" value="'+''+'"></td>';
        markup+='<td><input size="40" class="form-text" type="text" rel="val" value="'+''+'">'+'<p class="suggestion-custom">'+Drupal.t('for custom slots you can insert the following expression:DRUPAL.CUSTOM:{entity_type}:{field} <br/> e.g. DRUPAL.CUSTOM:commerce_product_variation:title')+'</p>'+'</td>';
        markup+='<td>'+'<span class="button remove-alexa-row-button">'+Drupal.t('remove row')+'</span>'+'</td>';
        markup+='</tr>';
      }
      markup+='</tbody></table>';
      markup+='<span class="button add-alexa-row-button" rel="alexa">'+Drupal.t('add new alexa slot')+'</span>';
      return markup;
    },
    getDrupalTable: function(data){
      let markup='<table class="bridge-table drupal-slots-table" rel="drupal">';
      markup+='<thead><tr>';
      markup+='<th>'+Drupal.t('Drupal variable name')+'</th>';
      markup+='<th>'+Drupal.t('Field path')+'</th>';
      markup+='<th>'+Drupal.t('Remove')+'</th>';
      markup+='</tr></thead><tbody>';
      //console.log(data);
      let existsrow=false;
      if(typeof data.drupal !== "undefined") {
        for (const [key, value] of Object.entries(data.drupal)) {
          //console.log(key, value);
          for (const [key2, value2] of Object.entries(value)) {
            //console.log(key2, value2);
            existsrow = true;
            markup += '<tr>';
            markup += '<td><input size="40" class="form-text" type="text" rel="key" value="' + key2 + '"></td>';
            markup += '<td><input size="40" class="form-text" type="text" rel="val" value="' + value2 + '"></td>';
            markup += '<td>' + '<span class="button remove-alexa-row-button">' + Drupal.t('remove row') + '</span>' + '</td>';
            markup += '</tr>';
          }
        }
      }
      if(!existsrow){
        markup+='<tr>';
        markup+='<td><input size="40" class="form-text" type="text" rel="key" value="'+''+'"></td>';
        markup+='<td><input size="40" class="form-text" type="text" rel="val" value="'+''+'"></td>';
        markup+='<td>'+'<span class="button remove-alexa-row-button">'+Drupal.t('remove row')+'</span>'+'</td>';
        markup+='</tr>';
      }
      markup+='</tbody></table>';
      markup+='<span class="button add-alexa-row-button" rel="drupal">'+Drupal.t('add new drupal slot')+'</span>';
      return markup;
    },
    getAssociationsTable: function(data){
      let markup='<table class="bridge-table associations-slots-table" rel="associations">';
      markup+='<thead><tr>';
      markup+='<th>'+Drupal.t('Source alexa slot name')+'</th>';
      markup+='<th>'+Drupal.t('Target Drupal variable name')+'</th>';
      markup+='<th>'+Drupal.t('Field path used as bridge')+'</th>';
      markup+='<th>'+Drupal.t('Remove')+'</th>';
      markup+='</tr></thead><tbody>';
      //console.log(data);
      let existsrow=false;
      if(typeof data.associations !== "undefined") {
        for (const [key, value] of Object.entries(data.associations)) {
          //console.log(key, value);
          existsrow = true;
          markup += '<tr>';
          markup += '<td><input size="40" class="form-text" type="text" rel="slot" value="' + value.slot + '"></td>';
          markup += '<td><input size="40" class="form-text" type="text" rel="target" value="' + value.target + '"></td>';
          markup += '<td><input size="40" class="form-text" type="text" rel="via" value="' + value.via + '"></td>';
          markup += '<td>' + '<span class="button remove-alexa-row-button">' + Drupal.t('remove row') + '</span>' + '</td>';
          markup += '</tr>';
        }
      }
      if(!existsrow){
        markup+='<tr>';
        markup+='<td><input size="40" class="form-text" type="text" rel="slot" value="'+''+'"></td>';
        markup+='<td><input size="40" class="form-text" type="text" rel="target" value="'+''+'"></td>';
        markup+='<td><input size="40" class="form-text" type="text" rel="via" value="'+''+'"></td>';
        markup+='<td>'+'<span class="button remove-alexa-row-button">'+Drupal.t('remove row')+'</span>'+'</td>';
        markup+='</tr>';
      }
      markup+='</tbody></table>';
      markup+='<span class="button add-alexa-row-button" rel="associations">'+Drupal.t('add new association')+'</span>';
      return markup;
    },
    addEvents: function(){
      Drupal.behaviors.alexaIntentEntityForm.addRowEvents();
      Drupal.behaviors.alexaIntentEntityForm.addChangeElementEvents();
    },
    updateSavedJson: function(table, rowindex, key, value){
      let actualData = null;
      if($('input[name="slots"]').val().length > 0) {
        actualData = JSON.parse($('input[name="slots"]').val());
      } else {
        actualData = JSON.parse('{"alexa" : [],"drupal" : [],"associations" :[]}');
      }
      //console.log(table);
      //console.log(rowindex);
      //console.log(key);
      //console.log(value);
      actualData[table] = [];
      if(table==='alexa' || table==='drupal') {
        $('table[rel="' + table + '"] tbody tr').each(function () {
          let keyactual = $(this).find('input[rel="key"]').val();
          let valactual = $(this).find('input[rel="val"]').val();
          //console.log(keyactual);
          //console.log(valactual);
          let newval = {};
          newval[keyactual] = valactual;
          //console.log("newval");
          //console.log(newval);
          actualData[table].push(newval);
        });
      } else {
        $('table[rel="' + table + '"] tbody tr').each(function () {
          let slotactual = $(this).find('input[rel="slot"]').val();
          let targetactual = $(this).find('input[rel="target"]').val();
          let viaactual = $(this).find('input[rel="via"]').val();
          //console.log(slotactual);
          //console.log(targetactual);
          //console.log(viaactual);
          let newvalvalactual = {
            'slot':slotactual,
            'target':targetactual,
            'via':viaactual
          };
          //console.log("newval");
          //console.log(newvalvalactual);
          actualData[table].push(newvalvalactual);
        });
      }
      console.log(actualData);
      $('input[name="slots"]').val(JSON.stringify(actualData));
    },
    addChangeElementEvents: function(){
      $('.bridge-table input').keyup(function(){
        let tableType = $(this).closest('table').attr('rel');
        //console.log('tableType ' + tableType);
        let index='';
        let key='';
        let val='';
        switch (tableType) {
          case 'alexa':
            //console.log('alexa');
            //console.log($(this).val());
            //console.log($('input[name="slots"]').val());
            index = $(this).closest('tr').index();
            key = $(this).closest('tr').find('input[rel="key"]').val();
            val = $(this).closest('tr').find('input[rel="val"]').val();
            Drupal.behaviors.alexaIntentEntityForm.updateSavedJson(tableType,index,key,val);
            break;
          case 'drupal':
            //console.log('drupal');
            //console.log($(this).val());
            //console.log($('input[name="slots"]').val());
            index = $(this).closest('tr').index();
            key = $(this).closest('tr').find('input[rel="key"]').val();
            val = $(this).closest('tr').find('input[rel="val"]').val();
            Drupal.behaviors.alexaIntentEntityForm.updateSavedJson(tableType,index,key,val);
            break;
          case 'associations':
            //console.log('associations');
            //console.log($(this).val());
            //console.log($('input[name="slots"]').val());
            let slot = $(this).closest('tr').find('input[rel="slot"]').val();
            let target = $(this).closest('tr').find('input[rel="target"]').val();
            let via = $(this).closest('tr').find('input[rel="via"]').val();
            Drupal.behaviors.alexaIntentEntityForm.updateSavedJson(tableType,index,slot,val);
            break;
        }
      });
    },
    addRowEvents: function(){
      $('.add-alexa-row-button').click(function(){
        $('.'+$(this).attr('rel')+'-slots-table').append($('.'+$(this).attr('rel')+'-slots-table tr:last').clone().find('input').val('').end());
        Drupal.behaviors.alexaIntentEntityForm.removeRowEvent();
        Drupal.behaviors.alexaIntentEntityForm.addChangeElementEvents();
      });
      Drupal.behaviors.alexaIntentEntityForm.removeRowEvent();
    },
    removeRowEvent: function(){
      $('.remove-alexa-row-button').click(function(){
        $(this).closest('tr').remove();
      });
    },
    manageChanges: function(){

    }
  };
})(jQuery);
