<?php

namespace Drupal\alexa_skill_manager;
use Drupal\alexa_skill_manager\Entity\AlexaNotificationMappingEntity;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use GuzzleHttp\ClientInterface;
use ZipArchive;

/**
 * Class AlexaSkillManagerService.
 */
class AlexaSkillManagerService implements AlexaSkillManagerServiceInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  protected $eventDispatcher;

  protected $logger;

  protected $config;
  /**
   * Constructs a new AlexaSkillManagerService object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ClientInterface $http_client) {
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $http_client;
    $this->eventDispatcher = \Drupal::service('event_dispatcher');
    $this->logger = \Drupal::logger('alexa_skill_manager');
    $this->config = \Drupal::config('alexa_skill_manager.alexaskillmanagerconfig');
  }

  public function loadSkillData(){
    $ret = [];
    $skills = $this->entityTypeManager
      ->getStorage('alexa_skill_entity')
      ->getQuery()
      ->execute();
    $skillsData = [];
    foreach($skills as $skill){
      $skillsData[] = $this->entityTypeManager
        ->getStorage('alexa_skill_entity')
        ->load($skill);
    }

    //take first skills data
    //@todo explode and support multiple intents
    //load publishing information for skill.json
    $publishingInformations = [];
    foreach($skillsData as $k => $v){
      /** @var \Drupal\alexa_skill_manager\Entity\AlexaSkillEntity $v */
      $publishingInformations[$v->getLanguage()] = [
        'summary' => $v->getSummary(),
        'invocation_name' => $v->getInvocationName(),
        'skill_description' => $v->getDescription(),
        'examplephrases' => $v->getExamplePhrases(),
        'name' => $v->getName(),
        'intents' => $v->getIntents()
      ];
    }

    $ret['publishinginformations'] = $publishingInformations;
    $ret['apltemplates'] = $this->retrieveAplTemplates();

    return $ret;
  }

  public function updateBaseJson($data) {
      $baseSkillJson = $this->getBaseSkillJson();
      $baseModelJson = $this->getBaseModelJson();
      $this->downloadLatestLambdaFolder();
      //generate models
      foreach($data['publishinginformations'] as $k => $v){
        $invocationName = $v['invocation_name'];
        //check if folder exists, if it doesn't we regenerate
        if(!is_dir(\Drupal::service('file_system')->getTempDirectory()."/drupalalexaskillmanager-master/models")){
          \Drupal::service('file_system')->mkdir(\Drupal::service('file_system')->getTempDirectory()."/drupalalexaskillmanager-master/models");
        }
        $path = \Drupal::service('file_system')->getTempDirectory()."/drupalalexaskillmanager-master/models/$k.json";
        if(file_exists($path)){
          unlink($path);
        }
        $handle = fopen($path, 'w') or die(t('Cannot open file:  ').$path);
        $baseModelJson->interactionModel->languageModel->invocationName = $invocationName;
        $types=[];
        foreach(explode(",",$v['intents']) as $key => $intent){
          $this->addIntent($baseModelJson->interactionModel->languageModel->intents,$intent,$types);
        }
        if(!empty($types)){
          $baseModelJson->interactionModel->languageModel->types = $types;
        }
        //manage intents
        //$baseModelJson->interactionModel->languageModel->intents[4]->samples = $askPricesSamples;
        fwrite($handle, json_encode($baseModelJson,JSON_PRETTY_PRINT));
      }
      foreach($data['apltemplates'] as $aplTemplate){
        if(!is_dir(\Drupal::service('file_system')->getTempDirectory()."/drupalalexaskillmanager-master/lambda/custom/src/common/apl")){
          \Drupal::service('file_system')->mkdir(\Drupal::service('file_system')->getTempDirectory()."/drupalalexaskillmanager-master/lambda/custom/src/common/apl");
        }
        $name = $aplTemplate->label();
        $path = \Drupal::service('file_system')->getTempDirectory()."/drupalalexaskillmanager-master/lambda/custom/src/common/apl/$name.json";
        if(file_exists($path)){
          unlink($path);
        }
        $handle = fopen($path, 'w') or die(t('Cannot open file:  ').$path);
        fwrite($handle, $aplTemplate->getStructure());
      }
      $this->updateSkillJson($baseSkillJson,$data);
  }

  private function downloadLatestLambdaFolder(){
    $pathToExtract = \Drupal::service('file_system')->getTempDirectory();
    $pathzip = \Drupal::service('file_system')->getTempDirectory()."/drupalalexaskillmanager-master.zip";
    $lambdaUrl = \Drupal::config('alexa_skill_manager.alexaskillmanagerconfig')->get('alexa_lambda_function_url');
    if(empty($lambdaUrl)){
      $lambdaUrl = 'https://github.com/Rafuel92/drupalalexaskillmanager/archive/master.zip';
    }
    file_put_contents($pathzip, file_get_contents($lambdaUrl));
    $zip = new ZipArchive;
    $res = $zip->open($pathzip);
    if ($res === TRUE) {
      $zip->extractTo($pathToExtract);
      $zip->close();
    } else {
      die('An error occurred');
    }
    $this->replaceAuthenticatedIntents();
  }

  public function generateZipAndGetData() {
    //replace skill.json, replace models per language and download zip
    $zip = new ZipArchive();
    $date = date('d-M-Y H:i:s');
    $filename = \Drupal::service('file_system')->getTempDirectory()."/alexaSkill-$date.zip";

    if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
      exit("cannot open <$filename>\n");
    }

    $dir = \Drupal::service('file_system')->getTempDirectory().'/drupalalexaskillmanager-master';

    $this->createZip($zip,\Drupal::service('file_system')->realpath($dir));

    $zip->close();

    return ['zip' => $zip,'name' => $filename];
  }

  private function createZip(ZipArchive &$zip,$dir){
    if (is_dir($dir)){

      if ($dh = opendir($dir)){
        while (($file = readdir($dh)) !== false){

          // If file
          if (is_file($dir.'/'.$file)) {
            if($file != '' && $file != '.' && $file != '..'){
              $toRemove = \Drupal::service('file_system')->realpath(\Drupal::service('file_system')->getTempDirectory().'/drupalalexaskillmanager-master');
              $path = str_replace($toRemove,'',$dir);
              $zip->addFile($dir.'/'.$file,$path.'/'.$file);
            }
          }else{
            // If directory
            if(is_dir($dir.'/'.$file) ){

              if($file != '' && $file != '.' && $file != '..'){
                $toRemove = \Drupal::service('file_system')->realpath(\Drupal::service('file_system')->getTempDirectory().'/drupalalexaskillmanager-master');
                $path = str_replace($toRemove,'',$dir);
                // Add empty directory
                $zip->addEmptyDir($path.'/'.$file);

                $folder = $dir.'/'.$file;

                // Read data of the folder
                $this->createZip($zip,$folder);
              }
            }

          }

        }
        closedir($dh);
      }
    }
  }

  public function generateResponseByIntentData(string $content) {
    if(empty($content)){
      return 'an error occurred';
    }
    $data = json_decode($content);
    $intentName = $data->name;
    $slots = $data->slots;
    $receivedSlots = [];
    \Drupal::logger('slots')->info(serialize($slots));
    if($slots){
      foreach((array)$slots as $k => $v){
        \Drupal::logger('slotsv')->info(serialize($v));
        $receivedSlots[$v->name] = $v->value;
      }
    }
    \Drupal::logger('calcslots')->info(serialize($receivedSlots));
    $calculated = $this->calcResponseByIntentNameAndSlots($intentName,$receivedSlots);
    if(!$calculated){
      return 'an error occurred';
    }
    return ['speak'=>$calculated['speak'],'apl'=>$this->retrieveAplTemplate($intentName,$receivedSlots,$calculated)];
  }



  public function calcResponseByIntentNameAndSlots(string $intentName, array $receivedSlots) {
    /** @var \Drupal\alexa_skill_manager\Entity\AlexaIntentEntity $intentEntity */
    $intentEntity = $this->loadIntentEntityByName($intentName);
    foreach($receivedSlots as $k => $v){
       $this->evaluateSynonyms($receivedSlots[$k]);
    }
    if(!$intentEntity){
      return FALSE;
    }
    //allow other modules to react on received feedback
    $this->eventDispatcher->dispatch(AlexaSkillManagerRespondEvent::RESPOND,new AlexaSkillManagerRespondEvent($intentEntity,$receivedSlots));
    $answer = $intentEntity->getAnswer();
    $configuratedSlots = json_decode($intentEntity->getSlots());
    $replaces = [];
    foreach($configuratedSlots->drupal as $drupalToken){
      $token = (array) $drupalToken;
      foreach($configuratedSlots->associations as $v){
        if($v->target != key($token)){
          continue;
        }
        if(empty($v->slot)){
          continue;
        }
        $assocSlotName = $v->slot;
        $assocViaEntityName = explode(':',$v->via)[0];
        $assocViaFieldName = explode(':',$v->via)[1];
        $baseVal = $receivedSlots[$assocSlotName];
        \Drupal::logger('alexa_skill_debug')->info(serialize([$assocViaEntityName,$assocViaFieldName,$baseVal]));
        $entity = $this->loadEntityByData($assocViaEntityName,$assocViaFieldName,$baseVal);
        $fieldToLoad = explode(':',current(array_values($token)))[1];
        if(isset($entity->{$fieldToLoad}) && !is_null($entity->{$fieldToLoad})) {
          $val = $entity->{$fieldToLoad}->getValue();
        } else {
          return t('Element not found');
        }
        if(isset($val[0]['currency_code'])){ //for prices
          $replaces[key($token)] = round($val[0]['number'],2) . " ". $val[0]['currency_code'] ;
        } else {
          $replaces[key($token)] = $val[0]['value'];
        }
      }
    }
    /**
     * replace all received tokens
     */
    foreach($receivedSlots as $k => $v){
      $answer = str_replace('{'.$k.'}',$v,$answer);
    }
    /**
     * replace all calculated tokens
     */
    foreach($replaces as $k => $v){
      $answer = str_replace('{'.$k.'}',$v,$answer);
    }
    return ['speak'=>$answer,'answer'=>$replaces];
  }

  public function loadIntentEntityByName(string $name) {
    $res = $this->entityTypeManager->getStorage('alexa_intent_entity')
      ->getQuery()
      ->condition('alexaintentname',$name)
      ->execute();
    if(empty($res)){
      return FALSE;
    }
    $intentData = NULL;
    foreach($res as $intent){
      $intentData = $this->entityTypeManager
        ->getStorage('alexa_intent_entity')
        ->load($intent);
    }
    return $intentData;
  }

  /**
   *
   * Loads an entity of type $assocViaEntityName with the field $assocViaFieldName that has $baseVal as value
   *
   * @param $assocViaEntityName
   * @param $assocViaFieldName
   * @param $baseVal
   */
  private function loadEntityByData($assocViaEntityName, $assocViaFieldName, $baseVal) {
    $res = $this->entityTypeManager->getStorage($assocViaEntityName)
      ->getQuery()
      ->condition($assocViaFieldName,$baseVal)
      ->execute();
    if(empty($res)){
      return FALSE;
    }
    $loaded = FALSE;
    foreach($res as $k => $v){
      $loaded = $this->entityTypeManager->getStorage($assocViaEntityName)->load($v);
    }
    return $loaded;

  }

  private function getBaseSkillJson() {
    $text = '{
  "manifest": {
    "publishingInformation": {
      "locales": {
        "en-US": {
          "summary": "{summary}",
          "examplePhrases": [
            "phrase1"
          ],
          "name": "name",
          "description": "description"
        }
      },
      "isAvailableWorldwide": true,
      "category": "%category%",
      "distributionCountries": []
    },
    "apis": {
      "custom": {
        "endpoint": {
          "uri": "%arnuri%"
        }
      }
    },
    "manifestVersion": "1.0"
  }
}
';
    $category = \Drupal::config('alexa_skill_manager.alexaskillmanagerconfig')->get('alexa_skill_category');
    if(is_null($category)) {
      //default KNOWLEDGE_AND_TRIVIA
      $text = str_replace('%category%', 'KNOWLEDGE_AND_TRIVIA', $text);
    } else {
      $text = str_replace('%category%', $category, $text);
    }
    $arnuri = \Drupal::config('alexa_skill_manager.alexaskillmanagerconfig')->get('alexa_arn');
    if(is_null($arnuri)) {
      //default KNOWLEDGE_AND_TRIVIA
      $text = str_replace('%arnuri%', '', $text);
    } else {
      $text = str_replace('%arnuri%', $arnuri, $text);
    }
    return json_decode($text);
  }

  private function getBaseModelJson(){
    return json_decode('{
          "interactionModel": {
              "languageModel": {
                  "invocationName": "{{invocationname}}",
                  "intents": [
                      {
                          "name": "AMAZON.CancelIntent",
                          "samples": []
                      },
                      {
                          "name": "AMAZON.HelpIntent",
                          "samples": []
                      },
                      {
                          "name": "AMAZON.StopIntent",
                          "samples": []
                      },
                      {
                          "name": "AMAZON.NavigateHomeIntent",
                          "samples": []
                      }
                  ],
                  "types": []
              }
          }
      }');
  }

  private function addIntent(&$intents, $intent,&$types) {
    $intentData = $this->loadIntentByMachineName($intent);
    if($intentData) {
      $intentToAdd = new \stdClass();
      $intentToAdd->name = $intentData->getAlexaIntentName();
      $intentToAdd->samples = explode("\r\n",$intentData->getSamples());
      /** @var \Drupal\alexa_skill_manager\Entity\AlexaIntentEntity $intentData */
      $slots = json_decode($intentData->getSlots());
      $slotsToAdd=[];
      foreach($slots->alexa as $slot){
        if(empty(current($slot))){
          continue;
        }
        $slotActual = new \stdClass();
        $slotActual->name = key($slot);
        $slotActual->type = current($slot);
        if(strpos($slotActual->type,'DRUPAL.CUSTOM')!==false){
          $this->populateCustomSlot($slotActual,$types);
        }
        $slotsToAdd[] = $slotActual;
      }
      $intentToAdd->slots = $slotsToAdd;
      $intents[] = $intentToAdd;
    }
  }

  private function populateCustomSlot(&$slotActual,&$types){
    $entityPath = str_replace('DRUPAL.CUSTOM:','',$slotActual->type);
    $entityMachineName = explode(':',$entityPath)[0];
    $entityField = explode(':',$entityPath)[1];
    $query = $this->entityTypeManager->getStorage($entityMachineName)
      ->getQuery();
    $res = $query->execute();
    $slotValues = [];
    foreach($res as $k => $v){
      $entity = $this->entityTypeManager->getStorage($entityMachineName)->load($v);
      $value = $entity->get($entityField)->getValue();
      if(isset($value[0]['value'])){
        $slotValues[] = $value[0]['value'];
      }
    }
    $slotTypeValuesFormatted = [];
    foreach($slotValues as $key => $slotValue){
      $actualValFormatted = new \StdClass();
      $actualValFormatted->id = $key;
      $actualName = new \StdClass();
      $actualName->value = $slotValue;
      $synonyms = $this->getSynonyms($slotValue);
      if(!empty($synonyms)) {
        $actualName->synonyms = $synonyms;
      }
      $actualValFormatted->name = $actualName;
      $slotTypeValuesFormatted[] = $actualValFormatted;
    }
    $newType = new \StdClass();
    $newType->name = 'custom'.$entityMachineName.$entityField;
    $newType->values = $slotTypeValuesFormatted;
    $types[] = $newType;
    $slotActual->type = 'custom'.$entityMachineName.$entityField;;
  }

  private function loadIntentByMachineName($intent) {
    return $this->entityTypeManager
      ->getStorage('alexa_intent_entity')
      ->load($intent) ?? false;
  }

  private function updateSkillJson($baseSkillJson, $data) {
    //generate skill.json
    $locales = new \StdClass();
    foreach($data['publishinginformations'] as $k => $v){
      $currentLocale = new \StdClass();
      $currentLocale->summary = $v['summary'];
      $currentLocale->description = $v['skill_description'];
      $currentLocale->name = $v['name'];
      foreach(explode("\r\n",$v['examplephrases']) as $phrase) {
        if(!empty($phrase)) {
          $currentLocale->examplePhrases[] = $phrase;
        }
      }
      $locales->{$k} = $currentLocale;
    }
    //check if file exists, if it doesn't we regenerate
    $path = \Drupal::service('file_system')->getTempDirectory()."/drupalalexaskillmanager-master/skill.json";
    if(file_exists($path)){
      unlink($path);
    }
    $handle = fopen($path, 'w') or die(t('Cannot open file:  ').$path);
    $baseSkillJson->manifest->publishingInformation->locales = $locales;
    //manage intents
    //$baseModelJson->interactionModel->languageModel->intents[4]->samples = $askPricesSamples;
    fwrite($handle, json_encode($baseSkillJson,JSON_PRETTY_PRINT));
  }

  private function getSynonyms($slotValue) {
    $synonyms=[];
    $res = $this->entityTypeManager->getStorage('alexa_synonym_entity')
      ->getQuery()
      ->condition('phrase',$slotValue)
      ->execute();
    foreach($res as $k => $v){
      /** @var \Drupal\alexa_skill_manager\Entity\AlexaSynonymEntity $synonym */
      $synonym = $this->entityTypeManager->getStorage('alexa_synonym_entity')->load($v);
      foreach(explode(",",$synonym->getSynonyms()) as $syn){
        $synonyms[] = trim($syn);
      }
    }
    return $synonyms;

  }

  private function getSourceBySynonym($slotValue) {
    $res = $this->entityTypeManager->getStorage('alexa_synonym_entity')
      ->getQuery()
      ->condition('synonyms',$slotValue,'CONTAINS')
      ->execute();
    foreach($res as $k => $v){
      /** @var \Drupal\alexa_skill_manager\Entity\AlexaSynonymEntity $synonym */
      $synonym = $this->entityTypeManager->getStorage('alexa_synonym_entity')->load($v);
      return $synonym->getPhrase();
    }
    return '';
  }

  private function evaluateSynonyms(&$sourcePhrase) {
    $source = $this->getSourceBySynonym($sourcePhrase);
    if(!empty($source)){
      $sourcePhrase = $source;
    }
  }

  private function replaceAuthenticatedIntents() {
    $path = \Drupal::service('file_system')->getTempDirectory().'/drupalalexaskillmanager-master/lambda/custom/src/intentHandlers/responderIntent.js';
    $original = file_get_contents($path);
    //replace authenticatedIntentsArray
    $authenticatedIntentsString = $this->retrieveAuthenticatedIntentsString();
    if(!empty($authenticatedIntentsString)) {
      $replaced = str_replace('\'%placeholder%\'', $authenticatedIntentsString, $original);
      file_put_contents($path,$replaced);
    }
  }

  public function retrieveAuthenticatedIntentsString(){
    $ret = '';
    $res = $this->entityTypeManager->getStorage('alexa_intent_entity')
      ->getQuery()
      ->execute();
    if(empty($res)){
      return FALSE;
    }
    $intentData = NULL;
    foreach($res as $intentId){
      /** @var \Drupal\alexa_skill_manager\Entity\AlexaIntentEntity $intentEntity */
      $intentEntity = $this->entityTypeManager->getStorage('alexa_intent_entity')->load($intentId);
      if($intentEntity->getAuth()){
        $name = $intentEntity->getAlexaIntentName();
        $ret .= "'$name',";
      }
    }
    if(!empty($ret)){
      $ret = rtrim($ret, ',');
    }
    return $ret;
  }

  private function retrieveAplTemplates() {
    $res = $this->entityTypeManager->getStorage('alexa_apl_entity')
      ->getQuery()
      ->execute();
    if(empty($res)){
      return FALSE;
    }
    $aplData = [];
    foreach($res as $aplEntity){
      $aplData[] = $this->entityTypeManager
        ->getStorage('alexa_apl_entity')
        ->load($aplEntity);
    }
    return $aplData;
  }

  /**
   * @param $intentName
   * @param $receivedSlots
   * @param $calculated
   *
   * @return array|bool
   */
  private function retrieveAplTemplate($intentName, $receivedSlots,$calculated): array|bool {
    $intentEntity = $this->loadIntentEntityByName($intentName);
    $aplTemplate = $intentEntity->getAplTemplate();
    if(!empty($aplTemplate) && $aplTemplate !== '_none') {
      $aplTemplateName = $this->entityTypeManager->getStorage('alexa_apl_entity')->load($intentEntity->getAplTemplate())->getStructure();
      //ottieni apl associato, restuitisci variables dai received slots
      return [
        'template' => $aplTemplateName,
        'variables' => $calculated['answer']
      ];
    }
    return false;
  }

  public function sendNotification($type,$elements){
    $client = $this->httpClient;
    $clientId = $this->config->get('notifications_client_id');
    $clientSecret = $this->config->get('notifications_client_secret');
    try {
      $result = $client->post('https://api.amazon.com/auth/o2/token',[
        'form_params' => [
          'grant_type' => 'client_credentials',
          'client_id' => $clientId,
          'client_secret' => $clientSecret,
          'scope' => 'alexa::proactive_events'
        ],
        'headers' => [
          'cache-control ' => 'no-cache',
          'content-type' => 'application/x-www-form-urlencoded'
        ]
      ]);
      $data = json_decode((string) $result->getBody(), TRUE);
      $token = $data['access_token'] ?? FALSE;
      $this->sendAplNotificationElement($token,$type,$elements);
    } catch (\Exception $e) {
      $this->logger->info(serialize($e->getMessage()));
    }
  }

  private function sendAplNotificationElement($token,$type,$elements){
    $client = $this->httpClient;
    $type = 'AMAZON.MessageAlert.Activated';
    try {
      $client->post(
        'https://api.eu.amazonalexa.com/v1/proactiveEvents/stages/development',[
          'body' => $this->retrieveNotificationJsonByType($type,$elements),
          'headers' => [
            'authorization' => "Bearer $token",
            'cache-control ' => 'no-cache',
            'Content-Type' => 'application/json'
          ]
        ]
      );
    } catch (\Exception $e) {
      $this->logger->info(serialize($e->getMessage()));
    }
  }

  private function retrieveNotificationJsonByType(string $type,$elements): string {
    $referenceId = uniqid();
    $dateTimestamp = date('Y-m-d\TH:i:s\Z');
    $dateExpire = date('Y-m-d\TH:i:s\Z',strtotime("now + 1 hour"));
    $audience = 'Multicast';
    $creator = "Andy";
    $count = 5;
    if($type === 'AMAZON.MessageAlert.Activated') {
      return '{
        "timestamp": "' . $dateTimestamp . '",
        "referenceId": "' . $referenceId . '",
        "expiryTime": "' . $dateExpire . '",
        "event": {
            "name": "'.$type.'",
            "payload": {
              "state": {
                "status": "UNREAD",
                "freshness": "'.$elements['state.freshness'].'"
              },
              "messageGroup": {
                "creator": {
                  "name": "'.$elements['messageGroup.creator.name'].'"
                },
                "count": 1
              }
            }
          },
        "relevantAudience": {
            "type": "' . $audience . '",
            "payload": {}
        }
    }';
    }
    return '';
  }

  /**
   * @return array
   */
  public function getNotificationsEnabledTypes(){
    $enabled = [];
    $all = \Drupal::entityQuery('alexa_notific_mapping_entity')->execute();
    foreach($all as $element){
      $entity = \Drupal::entityTypeManager('alexa_notific_mapping_entity')->getStorage('alexa_notific_mapping_entity')->load('amazon_message_alert_activated');
      $enabled[$entity->getContentType()] = $entity;
    }
    return $enabled;
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return void
   */
  public function notificationLogic(\Drupal\Core\Entity\EntityInterface $entity){
    $enabledTypes = $this->getNotificationsEnabledTypes();
    if(method_exists($entity,'getType') && array_key_exists($entity->getType(),$enabledTypes)){
      $this->sendNotificationByMappingAndNode($entity,$enabledTypes[$entity->getType()]);
    }
  }

  private function sendNotificationByMappingAndNode(\Drupal\Core\Entity\EntityInterface $entity, AlexaNotificationMappingEntity $type) {
    $mappingData = $type->getMappingData();
    $notificationType = $type->getAlexaType();
    if($type->getPublishedOnly() === (bool)$entity->status->value){
      $replacedElements = $this->retrieveReplacedElements($entity,$mappingData);
      $this->sendNotification($notificationType,$replacedElements);
    }
  }

  private function retrieveReplacedElements(\Drupal\Core\Entity\EntityInterface $entity, $mappingData) {
    $assoc = json_decode($mappingData);
    $replaced = [];
    foreach($assoc as $alexaName => $fieldName){
      if($fieldName === "author"){
        $replaced[$alexaName] = \Drupal::entityTypeManager()->getStorage('user')->load($entity->uid->target_id)->name->value;
      } else {
        $replaced[$alexaName] = $entity->{$fieldName}->value;
      }
    }
    return $replaced;
  }

}
