<?php

namespace Drupal\alexa_skill_manager;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Alexa Intent entities.
 */
class AlexaIntentEntityListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Alexa Intent');
    $header['id'] = $this->t('Machine name');
    $header['alexa_intent_name'] = $this->t('Alexa intent name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['alexa_intent_name'] = $entity->getAlexaIntentName();
    // You probably want a few more properties here...
    return $row + parent::buildRow($entity);
  }

}
