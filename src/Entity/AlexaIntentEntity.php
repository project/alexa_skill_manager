<?php

namespace Drupal\alexa_skill_manager\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Alexa Intent entity.
 *
 * @ConfigEntityType(
 *   id = "alexa_intent_entity",
 *   label = @Translation("Alexa Intent"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\alexa_skill_manager\AlexaIntentEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\alexa_skill_manager\Form\AlexaIntentEntityForm",
 *       "edit" = "Drupal\alexa_skill_manager\Form\AlexaIntentEntityForm",
 *       "delete" = "Drupal\alexa_skill_manager\Form\AlexaIntentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\alexa_skill_manager\AlexaIntentEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "alexa_intent_entity",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "alexaintentname" = "alexaintentname",
 *     "slots" = "slots",
 *     "samples" = "samples",
 *     "answer" = "answer",
 *     "auth" = "auth",
 *     "apltemplate" = "apltemplate"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/alexa_skill_manager/intents/alexa_intent_entity/{alexa_intent_entity}",
 *     "add-form" = "/admin/structure/alexa_skill_manager/intents/alexa_intent_entity/add",
 *     "edit-form" = "/admin/structure/alexa_skill_manager/intents/alexa_intent_entity/{alexa_intent_entity}/edit",
 *     "delete-form" = "/admin/structure/alexa_skill_manager/intents/alexa_intent_entity/{alexa_intent_entity}/delete",
 *     "collection" = "/admin/structure/alexa_skill_manager/intents/alexa_intent_entity"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "alexaintentname",
 *     "slots",
 *     "samples",
 *     "answer",
 *     "auth",
 *     "apltemplate"
 *   }
 * )
 */
class AlexaIntentEntity extends ConfigEntityBase implements AlexaIntentEntityInterface {

  /**
   * The Alexa Intent ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Alexa Intent label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Alexa Intent name.
   *
   * @var string
   */
  protected $alexaintentname;

  /**
   * The slot definitions as json.
   *
   * @var string
   */
  protected $slots;

  /**
   * The samples.
   *
   * @var string
   */
  protected $samples;


  /**
   * The answers.
   *
   * @var string
   */
  protected $answer;

  /**
   *
   * intent needs authentication?
   * @var
   */
  protected $auth;

  /** Associated apl template
   * @var string
   */
  protected $apltemplate;

  public function getAlexaIntentName(){
    return $this->alexaintentname;
  }

  public function getSlots() {
    return $this->slots;
  }

  public function getSamples() {
     return $this->samples;
  }

  public function getAnswer() {
      return $this->answer;
  }

  public function getAuth(){
    return $this->auth;
  }

  public function getAplTemplate(){
    return $this->apltemplate;
  }

}
