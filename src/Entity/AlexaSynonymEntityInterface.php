<?php

namespace Drupal\alexa_skill_manager\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Alexa synonym entities.
 */
interface AlexaSynonymEntityInterface extends ConfigEntityInterface {

  public function getSynonyms();

  public function getPhrase();

}
