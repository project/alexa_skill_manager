<?php

namespace Drupal\alexa_skill_manager\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Alexa skill entity.
 *
 * @ConfigEntityType(
 *   id = "alexa_skill_entity",
 *   label = @Translation("Alexa skill"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\alexa_skill_manager\AlexaSkillEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\alexa_skill_manager\Form\AlexaSkillEntityForm",
 *       "edit" = "Drupal\alexa_skill_manager\Form\AlexaSkillEntityForm",
 *       "delete" = "Drupal\alexa_skill_manager\Form\AlexaSkillEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\alexa_skill_manager\AlexaSkillEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "alexa_skill_entity",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "language" = "language",
 *     "summary" = "summary",
 *     "examplephrases" = "examplephrases",
 *     "name" = "name",
 *     "invocationname" = "invocationname",
 *     "description" = "description",
 *     "intents" = "intents"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/alexa_skill_manager/alexa_skill_entity/{alexa_skill_entity}",
 *     "add-form" = "/admin/structure/alexa_skill_manager/alexa_skill_entity/add",
 *     "edit-form" = "/admin/structure/alexa_skill_manager/alexa_skill_entity/{alexa_skill_entity}/edit",
 *     "delete-form" = "/admin/structure/alexa_skill_manager/alexa_skill_entity/{alexa_skill_entity}/delete",
 *     "collection" = "/admin/structure/alexa_skill_manager/alexa_skill_entity"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "language",
 *     "summary",
 *     "examplephrases",
 *     "name",
 *     "invocationname",
 *     "description",
 *     "intents"
 *   }
 * )
 */
class AlexaSkillEntity extends ConfigEntityBase implements AlexaSkillEntityInterface {

  /**
   * The Alexa skill ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Alexa skill label.
   *
   * @var string
   */
  protected $label;

  protected $language;

  protected $summary;

  protected $examplephrases;

  protected $name;

  protected $invocationname;

  protected $description;

  protected $intents;

  public function getLanguage(){
    return $this->language;
  }

  public function getSummary(){
    return $this->summary;
  }

  public function getExamplePhrases(){
    return $this->examplephrases;
  }

  public function getName(){
    return $this->name;
  }

  public function getInvocationName(){
    return $this->invocationname;
  }

  public function getDescription(){
    return $this->description;
  }

  public function getIntents(){
    return $this->intents;
  }

}
