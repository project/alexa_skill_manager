<?php

namespace Drupal\alexa_skill_manager\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Alexa notification mapping entity entity.
 *
 * @ConfigEntityType(
 *   id = "alexa_notific_mapping_entity",
 *   label = @Translation("Alexa notification mapping entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\alexa_skill_manager\AlexaNotificationMappingEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\alexa_skill_manager\Form\AlexaNotificationMappingEntityForm",
 *       "edit" = "Drupal\alexa_skill_manager\Form\AlexaNotificationMappingEntityForm",
 *       "delete" = "Drupal\alexa_skill_manager\Form\AlexaNotificationMappingEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\alexa_skill_manager\AlexaNotificationMappingEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "alexa_notific_mapping_entity",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "alexa_type" = "alexa_type",
 *     "content_type" = "content_type",
 *     "mapping_data" = "mapping_data",
 *     "published_only" = "published_only"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/alexa_skill_manager/notifications/alexa_notific_mapping_entity/{alexa_notific_mapping_entity}",
 *     "add-form" = "/admin/structure/alexa_skill_manager/notifications/alexa_notific_mapping_entity/add",
 *     "edit-form" = "/admin/structure/alexa_skill_manager/notifications/alexa_notific_mapping_entity/{alexa_notific_mapping_entity}/edit",
 *     "delete-form" = "/admin/structure/alexa_skill_manager/notifications/alexa_notific_mapping_entity/{alexa_notific_mapping_entity}/delete",
 *     "collection" = "/admin/structure/alexa_skill_manager/notifications/alexa_notific_mapping_entity"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "alexa_type",
 *     "content_type",
 *     "mapping_data",
 *     "published_only"
 *   },
 * )
 */
class AlexaNotificationMappingEntity extends ConfigEntityBase implements AlexaNotificationMappingEntityInterface {

  /**
   * The Alexa notification mapping entity ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Alexa notification mapping entity label.
   *
   * @var string
   */
  protected $label;

  protected $alexa_type;

  protected $content_type;

  protected $mapping_data;

  protected $published_only;

  public function getAlexaType() {
    return $this->alexa_type;
  }

  public function getContentType() {
    return $this->content_type;
  }

  public function getMappingData() {
    return $this->mapping_data;
  }

  public function getPublishedOnly() {
    return $this->published_only;
  }

}
