<?php

namespace Drupal\alexa_skill_manager\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Alexa synonym entity.
 *
 * @ConfigEntityType(
 *   id = "alexa_synonym_entity",
 *   label = @Translation("Alexa synonym"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\alexa_skill_manager\AlexaSynonymEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\alexa_skill_manager\Form\AlexaSynonymEntityForm",
 *       "edit" = "Drupal\alexa_skill_manager\Form\AlexaSynonymEntityForm",
 *       "delete" = "Drupal\alexa_skill_manager\Form\AlexaSynonymEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\alexa_skill_manager\AlexaSynonymEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "alexa_synonym_entity",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "phrase" = "phrase",
 *     "synonyms" = "synonyms"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/alexa_skill_manager/intents/alexa_synonym_entity/{alexa_synonym_entity}",
 *     "add-form" = "/admin/structure/alexa_skill_manager/intents/alexa_synonym_entity/add",
 *     "edit-form" = "/admin/structure/alexa_skill_manager/intents/alexa_synonym_entity/{alexa_synonym_entity}/edit",
 *     "delete-form" = "/admin/structure/alexa_skill_manager/intents/alexa_synonym_entity/{alexa_synonym_entity}/delete",
 *     "collection" = "/admin/structure/alexa_skill_manager/intents/alexa_synonym_entity"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "synonyms",
 *     "phrase"
 *   }
 * )
 */
class AlexaSynonymEntity extends ConfigEntityBase implements AlexaSynonymEntityInterface {

  /**
   * The Alexa synonym ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Alexa synonym label.
   *
   * @var string
   */
  protected $label;


  protected $synonyms;

  protected $phrase;


  public function getSynonyms(){
    return $this->synonyms;
  }

  public function getPhrase(){
    return $this->phrase;
  }

}
