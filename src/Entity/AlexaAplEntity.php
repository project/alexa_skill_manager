<?php

namespace Drupal\alexa_skill_manager\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Alexa APL Entity entity.
 *
 * @ConfigEntityType(
 *   id = "alexa_apl_entity",
 *   label = @Translation("Alexa APL Entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\alexa_skill_manager\AlexaAplEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\alexa_skill_manager\Form\AlexaAplEntityForm",
 *       "edit" = "Drupal\alexa_skill_manager\Form\AlexaAplEntityForm",
 *       "delete" = "Drupal\alexa_skill_manager\Form\AlexaAplEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\alexa_skill_manager\AlexaAplEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "alexa_apl_entity",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "structure" = "structure"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/alexa_apl_entity/{alexa_apl_entity}",
 *     "add-form" = "/admin/structure/alexa_apl_entity/add",
 *     "edit-form" = "/admin/structure/alexa_apl_entity/{alexa_apl_entity}/edit",
 *     "delete-form" = "/admin/structure/alexa_apl_entity/{alexa_apl_entity}/delete",
 *     "collection" = "/admin/structure/alexa_apl_entity"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "structure"
 *   }
 * )
 */
class AlexaAplEntity extends ConfigEntityBase implements AlexaAplEntityInterface {

  /**
   * The Alexa APL Entity ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Alexa APL Entity label.
   *
   * @var string
   */
  protected $label;

  /**
   *
   * json representing the structure
   *
   * @var string
   */
  protected $structure;

  /**
   * @return string
   */
  public function getStructure() {
    return $this->structure;
  }

}
