<?php

namespace Drupal\alexa_skill_manager\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Alexa Intent entities.
 */
interface AlexaIntentEntityInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
  public function getAlexaIntentName();
  public function getSlots();
  public function getSamples();
  public function getAnswer();
  public function getAuth();
}
