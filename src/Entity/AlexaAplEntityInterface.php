<?php

namespace Drupal\alexa_skill_manager\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Alexa APL Entity entities.
 */
interface AlexaAplEntityInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
