<?php

namespace Drupal\alexa_skill_manager\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AlexaSkillManagerConfigForm.
 */
class AlexaSkillManagerConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'alexa_skill_manager.alexaskillmanagerconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'alexa_skill_manager_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('alexa_skill_manager.alexaskillmanagerconfig');
    $form['alexa_lambda_function_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Alexa Lambda Function URL'),
      '#default_value' => $config->get('alexa_lambda_function_url'),
      '#description' => $this->t('default as <a href="@href">@href</a>',['@href'=>'https://github.com/Rafuel92/drupalalexaskillmanager/archive/master.zip'])
    ];
    $form['alexa_arn'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alexa ARN'),
      '#default_value' => $config->get('alexa_arn'),
      '#description' => $this->t('Lambda Function ARN, e.g. arn:aws:lambda:us-east-1:765070288978:function:ask-myskill-skill-default-1644999977417')
    ];
    $form['alexa_skill_category'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alexa Skill Category'),
      '#default_value' => $config->get('alexa_skill_category'),
      '#description' => $this->t("Category of the skill obtained from <a href='@href'>here</a>",['@href'=>'https://developer.amazon.com/it/docs/smapi/skill-manifest.html#category-enum'])
    ];
    $form['alexa_notifications_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Notifications client id'),
      '#default_value' => $config->get('notifications_client_id')
    ];
    $form['alexa_notifications_client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Notifications client secret'),
      '#default_value' => $config->get('notifications_client_secret')
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('alexa_skill_manager.alexaskillmanagerconfig')
      ->set('alexa_lambda_function_url', $form_state->getValue('alexa_lambda_function_url'))
      ->set('alexa_skill_category', $form_state->getValue('alexa_skill_category'))
      ->set('alexa_arn', $form_state->getValue('alexa_arn'))
      ->set('notifications_client_id', $form_state->getValue('alexa_notifications_client_id'))
      ->set('notifications_client_secret', $form_state->getValue('alexa_notifications_client_secret'))
      ->save();
  }

}
