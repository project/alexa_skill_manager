<?php

namespace Drupal\alexa_skill_manager\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class AlexaAplEntityForm.
 */
class AlexaAplEntityForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $alexa_apl_entity = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $alexa_apl_entity->label(),
      '#description' => $this->t("Name of the generated APL file"),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $alexa_apl_entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\alexa_skill_manager\Entity\AlexaAplEntity::load',
      ],
      '#disabled' => !$alexa_apl_entity->isNew(),
    ];

    $form['structure'] = [
      '#type' => 'textarea',
      '#title' => $this->t('APL structure'),
      '#rows' => 15,
      '#default_value' => $alexa_apl_entity->getStructure(),
      '#description' => $this->t('JSON representing the APL structure, for more info check <a href="@href">@href</a>',['@href'=>'https://developer.amazon.com/en-US/docs/alexa/alexa-presentation-language/use-apl-with-ask-sdk.html']),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $alexa_apl_entity = $this->entity;
    $status = $alexa_apl_entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Alexa APL Entity.', [
          '%label' => $alexa_apl_entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Alexa APL Entity.', [
          '%label' => $alexa_apl_entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($alexa_apl_entity->toUrl('collection'));
  }

}
