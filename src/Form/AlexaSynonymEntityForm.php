<?php

namespace Drupal\alexa_skill_manager\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class AlexaSynonymEntityForm.
 */
class AlexaSynonymEntityForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\alexa_skill_manager\Entity\AlexaSynonymEntity $alexa_synonym_entity */
    $alexa_synonym_entity = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $alexa_synonym_entity->label(),
      '#description' => $this->t("Label for the Alexa synonym."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $alexa_synonym_entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\alexa_skill_manager\Entity\AlexaSynonymEntity::load',
      ],
      '#disabled' => !$alexa_synonym_entity->isNew(),
    ];

    $form['phrase'] = [
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => $this->t('Phrase'),
      '#default_value' => $alexa_synonym_entity->getPhrase(),
      '#description' => $this->t("phrase for synonyms association"),
      '#required' => TRUE,
    ];

    $form['synonyms'] = [
      '#type' => 'textarea',
      '#maxlength' => 255,
      '#title' => $this->t('Synonyms'),
      '#default_value' => $alexa_synonym_entity->getSynonyms(),
      '#description' => $this->t("synonyms to associate separated by comma"),
      '#required' => TRUE,
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $alexa_synonym_entity = $this->entity;
    $status = $alexa_synonym_entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Alexa synonym.', [
          '%label' => $alexa_synonym_entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Alexa synonym.', [
          '%label' => $alexa_synonym_entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($alexa_synonym_entity->toUrl('collection'));
  }

}
