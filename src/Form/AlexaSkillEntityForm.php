<?php

namespace Drupal\alexa_skill_manager\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class AlexaSkillEntityForm.
 */
class AlexaSkillEntityForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\alexa_skill_manager\Entity\AlexaSkillEntity $alexa_skill_entity */
    $alexa_skill_entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $alexa_skill_entity->label(),
      '#description' => $this->t("Label for the Alexa skill."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $alexa_skill_entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\alexa_skill_manager\Entity\AlexaSkillEntity::load',
      ],
      '#disabled' => !$alexa_skill_entity->isNew(),
    ];

    $allowedLanguages = [
      'en-US',
      'en-AU',
      'en-CA',
      'en-IN',
      'en-GB',
      'de-DE',
      'es-ES',
      'es-MX',
      'fr-FR',
      'it-IT',
      'ja-JP',
      'fr-CA',
      'pt-BR',
      'hi-IN',
      'es-US'
    ];

    $form['language'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Language'),
      '#maxlength' => 255,
      '#default_value' => $alexa_skill_entity->getLanguage(),
      '#description' => $this->t("Language for the Alexa skill. Allowed language codes at the moment: ") . implode(", ",$allowedLanguages),
      '#required' => TRUE,
    ];



    $form['summary'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Summary'),
      '#maxlength' => 255,
      '#default_value' => $alexa_skill_entity->getSummary(),
      '#description' => $this->t("Summary for the Alexa skill."),
      '#required' => TRUE,
    ];

    $form['examplephrases'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Example Phrases'),
      '#default_value' => $alexa_skill_entity->getExamplePhrases(),
      '#description' => $this->t("Example Phrases for the Alexa skill."),
      '#required' => TRUE,
    ];

    $form['name'] = [
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => $this->t('Name'),
      '#default_value' => $alexa_skill_entity->getName(),
      '#description' => $this->t("Name to show on amazon store for the Alexa skill."),
      '#required' => TRUE,
    ];

    $form['invocationname'] = [
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => $this->t('Invocation Name'),
      '#default_value' => $alexa_skill_entity->getInvocationName(),
      '#description' => $this->t("Invocation name for the Alexa skill."),
      '#required' => TRUE,
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $alexa_skill_entity->getDescription(),
      '#description' => $this->t("Description on amazon store for the Alexa skill."),
      '#required' => TRUE,
    ];
    $intentOptions = [];

    $res = $this->entityTypeManager
      ->getStorage('alexa_intent_entity')
      ->getQuery()
      ->execute();
    if($res){
      $intentOptions = $res;
    }

    $form['intents'] = [
      '#type' => 'select',
      '#options' => $intentOptions,
      '#multiple' => TRUE,
      '#title' => $this->t('Enabled intents'),
      '#default_value' => explode(',',$alexa_skill_entity->getIntents()),
      '#description' => $this->t("Enabled intents available in drupal, they can be added  <a href='@url'>here</a>.",['@url'=>Url::fromRoute('entity.alexa_intent_entity.collection')->toString()]),
      '#required' => TRUE,
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $alexa_skill_entity = $this->entity;

    $alexa_skill_entity->set('invocationname',$form_state->getValue('invocationname'));
    $alexa_skill_entity->set('intents',implode(',',$form_state->getValue('intents')));

    $status = $alexa_skill_entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Alexa skill.', [
          '%label' => $alexa_skill_entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Alexa skill.', [
          '%label' => $alexa_skill_entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($alexa_skill_entity->toUrl('collection'));
  }

}
