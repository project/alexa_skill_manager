<?php

namespace Drupal\alexa_skill_manager\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class AlexaIntentEntityForm.
 */
class AlexaIntentEntityForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\alexa_skill_manager\Entity\AlexaIntentEntity $alexa_intent_entity */
    $alexa_intent_entity = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $alexa_intent_entity->label(),
      '#description' => $this->t("Label for the Alexa Intent."),
      '#required' => TRUE,
    ];

    $form['auth'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Authentication enabled'),
      '#default_value' => $alexa_intent_entity->getAuth(),
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $alexa_intent_entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\alexa_skill_manager\Entity\AlexaIntentEntity::load',
      ],
      '#disabled' => !$alexa_intent_entity->isNew(),
    ];

    $form['alexaintentname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alexa Intent Name'),
      '#maxlength' => 255,
      '#default_value' => $alexa_intent_entity->getAlexaIntentName(),
      '#description' => $this->t("Intent Name for the alexa console"),
      '#required' => TRUE,
    ];

    $exampleJson = '{
 "alexa" : [{"product":"AMAZON.SearchQuery"}],
 "drupal" : [{"returnedprice" : "commerce_product_variation:price"}],
 "associations" :[{"slot":"product","target":"returnedprice","via":"commerce_product_variation:title"}]
}';

    $form['slots'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Slots JSON'),
      '#default_value' => $alexa_intent_entity->getSlots(),
      '#description' => $this->t("Slots data for drupal and alexa in JSON, example:")."<br/>".$exampleJson,
      '#required' => TRUE,
    ];

    $form['samples'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Samples'),
      '#default_value' => $alexa_intent_entity->getSamples(),
      '#description' => $this->t("Insert one sample per line, use the tokens defined previously in the \"Drupal Variable Name\" field. Sample utterances can consist of only unicode characters, spaces, periods for abbreviations, underscores, possessive apostrophes, and hyphens."),
      '#required' => TRUE,
    ];

    $form['answer'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Answer'),
      '#default_value' => $alexa_intent_entity->getAnswer(),
      '#description' => $this->t("Insert the answer provided by drupal, use the tokens defined previously in the \"alexa slot name\" field"),
      '#required' => TRUE,
    ];

    $aplOptions = ['_none'=>$this->t('none')];
    $res = $this->entityTypeManager
      ->getStorage('alexa_apl_entity')
      ->getQuery()
      ->execute();
    if($res){
      $aplOptions += $res;
    }

    $form['apltemplate'] = [
      '#type' => 'select',
      '#options' => $aplOptions,
      '#title' => $this->t('Enabled APL Template'),
      '#default_value' => explode(',',$alexa_intent_entity->getAplTemplate()),
      '#description' => $this->t('Enabled APL Template')
    ];

    $form['#attached']['library'][] = 'alexa_skill_manager/alexaIntentEntityForm';

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $alexa_intent_entity = $this->entity;
    $alexa_intent_entity->set('alexaintentname',$form_state->getValue('alexaintentname'));
    $alexa_intent_entity->set('apltemplate',$form_state->getValue('apltemplate'));

    $status = $alexa_intent_entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Alexa Intent.', [
          '%label' => $alexa_intent_entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Alexa Intent.', [
          '%label' => $alexa_intent_entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($alexa_intent_entity->toUrl('collection'));
  }

}
