<?php

namespace Drupal\alexa_skill_manager\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class AlexaNotificationMappingEntityForm.
 */
class AlexaNotificationMappingEntityForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\alexa_skill_manager\Entity\AlexaNotificationMappingEntity $alexa_notific_mapping_entity */
    $alexa_notific_mapping_entity = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $alexa_notific_mapping_entity->label(),
      '#description' => $this->t("Label for the Alexa notification mapping entity."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $alexa_notific_mapping_entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\alexa_skill_manager\Entity\AlexaNotificationMappingEntity::load',
      ],
      '#disabled' => !$alexa_notific_mapping_entity->isNew(),
    ];

    $form['alexa_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alexa notification type'),
      '#default_value' => explode(',',$alexa_notific_mapping_entity->getAlexaType()),
      '#description' => $this->t("Alexa notification types are available at  <a href='@url'>https://developer.amazon.com/en-US/docs/alexa/smapi/schemas-for-proactive-events.html</a>. e.g. AMAZON.MessageAlert.Activated",['@url'=>'https://developer.amazon.com/en-US/docs/alexa/smapi/schemas-for-proactive-events.html']),
    ];

    $form['content_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Content type machine name'),
      '#default_value' => explode(',',$alexa_notific_mapping_entity->getContentType()),
      '#description' => $this->t("e.g. article"),
    ];

    $form['mapping_data'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Mapping Data'),
      '#default_value' => $alexa_notific_mapping_entity->getMappingData(),
      '#description' => $this->t('e.g. for AMAZON.MessageAlert.Activated {"state.freshness":"field_body","messageGroup.creator.name":"author"}')
    ];

    $form['published_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Published only'),
      '#default_value' => $alexa_notific_mapping_entity->getPublishedOnly(),
      '#description' => $this->t('If checked notification are sent only if the entity is published')
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $alexa_notific_mapping_entity = $this->entity;
    $alexa_notific_mapping_entity->set('alexa_type',$form_state->getValue('alexa_type'));
    $alexa_notific_mapping_entity->set('content_type',$form_state->getValue('content_type'));
    $alexa_notific_mapping_entity->set('mapping_data',$form_state->getValue('mapping_data'));
    $alexa_notific_mapping_entity->set('published_only',$form_state->getValue('published_only'));
    $status = $alexa_notific_mapping_entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Alexa notification mapping entity.', [
          '%label' => $alexa_notific_mapping_entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Alexa notification mapping entity.', [
          '%label' => $alexa_notific_mapping_entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($alexa_notific_mapping_entity->toUrl('collection'));
  }

}
