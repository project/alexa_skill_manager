<?php

namespace Drupal\alexa_skill_manager;

/**
 * Interface AlexaSkillManagerServiceInterface.
 */
interface AlexaSkillManagerServiceInterface {
  public function loadSkillData();

  public function updateBaseJson($data);

  public function generateZipAndGetData();

  public function generateResponseByIntentData(string $content);

}
