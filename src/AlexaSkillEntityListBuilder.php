<?php

namespace Drupal\alexa_skill_manager;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Alexa skill entities.
 */
class AlexaSkillEntityListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Alexa skill');
    $header['id'] = $this->t('Machine name');
    $header['language'] = $this->t('Alexa Language code');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['language'] = $entity->getLanguage();
    // You probably want a few more properties here...
    return $row + parent::buildRow($entity);
  }

}
