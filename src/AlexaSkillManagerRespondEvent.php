<?php


namespace Drupal\alexa_skill_manager;

use Drupal\alexa_skill_manager\Entity\AlexaIntentEntity;
use Symfony\Contracts\EventDispatcher\Event;


class AlexaSkillManagerRespondEvent extends Event {
  const RESPOND = 'alexa_skill_manager.respond';

  /** @var AlexaIntentEntity $intent */
  protected $intent;

  /** @var array $receivedSlots */
  protected $slots;

  public function __construct(AlexaIntentEntity $intentEntity, array $receivedSlots) {
    $this->intent = $intentEntity;
    $this->slots = $receivedSlots;
  }

  public function getIntent() {
    return $this->intent;
  }

  public function getSlots() {
    return $this->slots;
  }


}
