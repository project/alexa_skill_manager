<?php

namespace Drupal\alexa_skill_manager\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\alexa_skill_manager\AlexaSkillManagerServiceInterface;

/**
 * Class DownloadController.
 */
class DownloadController extends ControllerBase {

  /**
   * Drupal\alexa_skill_manager\AlexaSkillManagerServiceInterface definition.
   *
   * @var \Drupal\alexa_skill_manager\AlexaSkillManagerServiceInterface
   */
  protected $alexaSkillManagerManager;


  /**
   * Constructs a new DownloadController object.
   */
  public function __construct(AlexaSkillManagerServiceInterface $alexa_skill_manager_manager) {
    $this->alexaSkillManagerManager = $alexa_skill_manager_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('alexa_skill_manager.manager')
    );
  }

  /**
   * Download.
   *
   *   Return Downloaded skill.
   */
  public function download() {
    $data = $this->alexaSkillManagerManager->loadSkillData();
    $this->alexaSkillManagerManager->updateBaseJson($data);
    /** @var \ZipArchive $zip */
    $zipData = $this->alexaSkillManagerManager->generateZipAndGetData();
    $filename = $zipData['name'];
    header('Content-Type: application/zip');
    header('Content-Disposition: attachment; filename="'.basename($filename).'"');
    header('Content-Length: ' . filesize($filename));
    flush();
    readfile($filename);
    // delete file
    unlink($filename);
    $pathzip = \Drupal::service('file_system')->getTempDirectory()."/drupalalexaskillmanager-master.zip";
    unlink($pathzip);
    $pathskill = \Drupal::service('file_system')->getTempDirectory()."/drupalalexaskillmanager-master";
    $this->deleteFolder($pathskill);
    exit();


  }

  private function deleteFolder($dir){
    if (is_dir($dir)) {
      $objects = scandir($dir);
      foreach ($objects as $object) {
        if ($object != "." && $object != "..") {
          if (is_dir($dir."/".$object) && !is_link($dir."/".$object))
            $this->deleteFolder($dir."/".$object);
          else
            unlink($dir."/".$object);
        }
      }
      rmdir($dir);
    }
  }

}
