<?php

namespace Drupal\alexa_skill_manager\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\alexa_skill_manager\AlexaSkillManagerServiceInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class RespondController.
 */
class RespondController extends ControllerBase {

  /**
   * Drupal\alexa_skill_manager\AlexaSkillManagerServiceInterface definition.
   *
   * @var \Drupal\alexa_skill_manager\AlexaSkillManagerServiceInterface
   */
  protected $alexaSkillManagerManager;


  /**
   * Constructs a new RespondController object.
   */
  public function __construct(AlexaSkillManagerServiceInterface $alexa_skill_manager_manager) {
    $this->alexaSkillManagerManager = $alexa_skill_manager_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('alexa_skill_manager.manager')
    );
  }

  /**
   * Respond.
   *
   */
  public function respond() {
    $content = \Drupal::request()->getContent();
    \Drupal::logger('alexaSkillManagerRespond')->info(serialize(json_decode($content)));
     return new JsonResponse(['output'=>$this->alexaSkillManagerManager->generateResponseByIntentData($content)]);
  }

}
