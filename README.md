Prerequisites:
  - install the ask cli (https://developer.amazon.com/it/docs/smapi/ask-cli-command-reference.html#new-command)
Steps to generate and publish the skill:
1. create an initial skill project normally the ask command line utility with "ask new" to generate a new alexa skill project (Select Node.js as language, and hello world as template) 
2. fill in your intents data and responses at admin/structure/alexa_skill_manager/intents/alexa_intent_entity
3. fill in your skills data at admin/structure/alexa_skill_manager/alexa_skill_entity
3. fill in your skills data at admin/structure/alexa_skill_manager/alexa_skill_entity
4. (optional) fill in your synonyms at admin/structure/alexa_skill_manager/intents/alexa_synonym_entity
5. click on "Download skill" button at admin/structure/alexa_skill_manager/alexa_skill_entity
6. unpack the generated .zip archive 
7. replace the lambda folder of the new project with the downloaded lambda folder
8. replace the skill.json file of the new project with the downloaded skill.json
9. replace the models folder of the new project with the just downloaded models folder
10. setup the config variables (hostname, protocol) in lambda/custom/config.js
11. use the ask deploy command to deploy the skill in the alexa console
12. follow the standard alexa skill publishing workflow
